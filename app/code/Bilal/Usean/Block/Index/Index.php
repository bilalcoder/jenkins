<?php
/**
 * Collection examples
 * Copyright (C) 2019 @2019 
 * 
 * This file included in Bilal/Usean is licensed under OSL 3.0
 * 
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Bilal\Usean\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_productRepository;
    protected $_filter;
    protected $_searchCriteria;
    protected $_filterGroup;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\View\Element\Template\Context $context, 
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,
        \Magento\Framework\Api\Filter $filter,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        array $data = []       
    ) {
        $this->_productRepository = $productRepositoryInterface;
        $this->_searchCriteria = $searchCriteria;
        $this->_filter = $filter;
        parent::__construct($context, $data);
    }
    
    public function getProducts() {
        $this->_filter->setField("name")->setValue("%Joust%")->setConditionType("like");
        $filterGroup = $this->_filterGroup->setFilters([$this->_filter]);
        $searchCriteria = $this->_searchCriteria->setFilterGroups($filterGroup); 
        $products = $this->_productRepository->getList($searchCriteria);
        return $products;
    }
}
